# apt-installed

*apt-installed* is a simple way to see what programs you have explicitly installed
onto your Ubuntu build and not removed. No dependencies or removed programs are 
shown, and the output is displayed as lines of apt-get install commands for ease
of creating a 'install freeze' file. These commands are sorted in the order
that they were originally run, with programs installed at the same time 
retaining their grouping.

## Example Output

```shell
[spease@local:~/projects/apt-installed]$ python3 apt-installed.py
apt-get install gpaste
apt-get install lib32ncurses5
apt-get install steam-devices nvidia-driver-430
apt-get install whois
apt-get install libsdl-dev
apt-get install winehq-stable winehq-devel winehq-staging
apt-get install build-essential gcc g++ automake git-core autoconf make patch libtool libssl-dev grep binutils zlibc libc6 libbz2-dev cmake
apt-get install lutris
```
