#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gzip
from os import listdir
from os.path import isfile, join


def checkFile(f, history):
    for line in f:
        if '--upgrade' in line:
            continue
        apps = [x for x in line.split()[3:] if not x.startswith('-') and
                not x.startswith('DPkg::options')]
        if 'Commandline: apt-get install ' in line or 'Commandline: apt install' in line:
            history.append(apps)
        if 'Commandline: apt-get remove ' in line or 'Commandline: apt remove' in line:
            for app in apps:
                for i, x in reversed(list(enumerate(history))):
                    if app in x:
                        history[i].remove(app)
                        if not history[i]:
                            del history[i]
                        break
    return history


def sort_files(file):
    return -1*int(''.join([s for s in file if s.isdigit()]) or 0)


path = '/var/log/apt/'
history_files = [f for f in listdir(path) if isfile(join(path, f))
                 and f.startswith('history.log')]
# sort, oldest first.
history_files = sorted(history_files, key=sort_files)

history = []
for history_file in history_files:
    try:
        with gzip.open(join(path, history_file), 'rt') as f:
            history = checkFile(f, history)
    except OSError:
        with open(join(path, history_file), 'r') as f:
            history = checkFile(f, history)
for command in history:
    print('apt-get install ' + ' '.join(command))
